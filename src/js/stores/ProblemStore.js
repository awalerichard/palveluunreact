import { observable } from "mobx";
import problem from "./models/problem";

export class ProblemStore {
  @observable problems = []
  @observable filter = ""

  createProblem = (value) => {
    this.problems.push(new Todo(value))
  }

  fetchProblems = () => {
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          var json = JSON.parse(xhr.responseText);
          json.forEach(function(item) {
            self.problems.push(new problem(item));
          });
        } else {
          console.log('err', xhr)
        }
      }
    };
    xhr.open('GET', "http://localhost:8181/consumer");
    xhr.send();
  }

}

export default new ProblemStore

