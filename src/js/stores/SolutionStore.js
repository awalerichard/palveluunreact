import { observable } from "mobx";
import Solution from "./models/Solution";

export class SolutionStore {
  @observable solutions = []
  @observable filter = ""

  createSolution = (value) => {
    this.solutions.push(new Todo(value))
  }

  

  fetchSolutions = () => {
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          var json = JSON.parse(xhr.responseText);
          json.forEach(function(item) {
            self.solutions.push(new Solution(item));
          });
        } else {
          console.log('err', xhr)
        }
      }
    };
    xhr.open('GET', "http://localhost:8181/consumer");
    xhr.send();
  }

}

export default new SolutionStore

