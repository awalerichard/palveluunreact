import { observable } from "mobx"

export default class Problem {
  @observable _id
  @observable first_name
  @observable last_name
  @observable email
  @observable phone
  @observable city
  @observable address 
  @observable postal
  @observable country
  @observable preferable_contact_media
  @observable prefereable_time
  @observable avatar

  constructor(params) {
    for(var key in params) {
        this[key] = params[key]
    }
  }

  deleteProblem = (_id) => {
  }

  updateProblem = (_id) => {

  }

  fetch = () => {
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          self.first_name = "Prataksha";
          self.last_name = "Gurung";
        } else {
          console.log('err', xhr);
        }
      }
    };
    xhr.open('GET', "http://localhost:8181/consumer/"+self._id);
    xhr.send();
  }
  
}
