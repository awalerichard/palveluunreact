//import "../css/main.css"

import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Body from "./pages/Body";
import Layout from "./pages/Layout";
//import SingleProblem from "./pages/SingleProblem";
//import SingleSolution from "./pages/SingleSolution";
//import Provider from "./pages/Provider";
//import Consumer from "./pages/Consumer";

const app = document.getElementById('app');
ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={Body}></IndexRoute>
    </Route>
  </Router>,
app);