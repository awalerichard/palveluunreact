import React from "react";

import Footer from "./Footer";
import Header from "./Header";
import Body from "./Body";

export default class Layout extends React.Component {
  render() {
    const { location } = this.props;
    const containerStyle = {
      marginTop: "60px"
    };
    console.log("layout");
    return (
        <div>
            <Header></Header>
            {this.props.children}
            <Footer></Footer>
        </div>
    );
  }
}
