import React from "react";
import ProblemContainer from "../components/ProblemContainer"
import SolutionContainer from "../components/SolutionContainer"

export default class Body extends React.Component {

  render() {

    return (
      <div>
        <SolutionContainer></SolutionContainer>
        <ProblemContainer></ProblemContainer>
      </div>
    );
  }
}
