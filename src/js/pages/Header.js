import React from "react";
import Nav from "../components/Nav";
import HeaderAd from "../components/HeaderAd";
import HeaderSearch from "../components/HeaderSearch";

export default class Header extends React.Component {
  render() {
    return (
      <div>
        <Nav></Nav>
        <HeaderAd></HeaderAd>
        <HeaderSearch></HeaderSearch>
      </div>
    );
  }
}
