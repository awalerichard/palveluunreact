import React from "react";

export default class Footer extends React.Component {
  render() {
    return (
      <footer id="footer">
        <div class="footer-widget">
          <div class="container">
            <div class="row">
              <div class="col-sm-2">
                <div class="single-widget">
                  <h2>Palvelut</h2>
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">Online Tuki</a></li>
                    <li><a href="#">Ota meihin yhteyttä</a></li>
                    <li><a href="#">Tilauksen tila</a></li>
                    <li><a href="#">Muuta sijaintia</a></li>
                    <li><a href="#">Yleisimmin kysytyt kysymykset</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="single-widget">
                  <h2>Ehdot</h2>
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">Käyttöehdot</a></li>
                    <li><a href="#">Tietosuojakäytäntö</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="single-widget">
                  <h2>Meistä</h2>
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">Yrityksen tiedot</a></li>
                    <li><a href="#">Urat</a></li>
                    <li><a href="#">Tekijänoikeus </a></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-3 col-sm-offset-1 pull-right">
                <div class="single-widget">
                  <h2>Liity postituslistaamme</h2>
                  <form action="#" class="searchform">
                    <input type="text" placeholder="Sähköpostiosoitteesi" /><br /><br />
                    <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                    <p>Vastaanota viimeisimmät päivitykset sivustoltamme.</p>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom">
        </div>
      </footer>
    );
  }
}
