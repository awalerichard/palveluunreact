import React from "react";
import { IndexLink, Link } from "react-router";
//import observer from "mobx-react";

//@observer
export default class SolutionContainer extends React.Component {

    render() {
        /*
        const {solutions} = this.props.store
        const solutionsList = consumers.map(solution => (
            <div key={solution._id} class="pad_right col-sm-6">
                <div class="box product-image-wrapper">
                    <div class="single-products row">
                        <div class="productinfo col-sm-4 ">
                            <img src="images/companyoffers/company1.png" alt="" />
                            <br />
                            <br />
                            <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i style="color:green;" class="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                        </div>
                        <div class="productinfo col-sm-5">
                            <h2>Matkapuhelimen näytön korjaus</h2> Tarjoamme matkapuhelimen näytön korjauksen 30% alennuksella.
                                            <br />
                            <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>

                        </div>

                        <div class="product-right-info col-sm-3">
                            <p>
                                Oulu, 90130 Finland
                                                <br />
                                <br /> Matkapuhelimet, Elektroniikka, Antennit.
                                            </p>
                            <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                        </div>
                    </div>
                    <hr />
                    <div class="single-products">
                        <div class="pull-left col-sm-8">
                            <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                            <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                            <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                        </div>
                        <div class="pull-right col-sm-4">
                            <a href="singleJobs.html" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                        </div>
                    </div>
                </div>
            </div>
        ))
        */
        return (
            <div class="recommended_items">
                <h2 class="title text-center">Yritysten tarjoukset</h2>

                <div class="viewmore1">
                    <div class="pad_right col-sm-6">
                        <div class="box product-image-wrapper">
                            <div class="single-products row">
                                <div class="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company1.png" alt="" />
                                    <br />
                                    <br />
                                    <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i style="color:green;" class="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                                </div>
                                <div class="productinfo col-sm-5">
                                    <h2>Matkapuhelimen näytön korjaus</h2> Tarjoamme matkapuhelimen näytön korjauksen 30% alennuksella.
                                            <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>

                                </div>

                                <div class="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Matkapuhelimet, Elektroniikka, Antennit.
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div class="single-products">
                                <div class="pull-left col-sm-8">
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div class="pull-right col-sm-4">
                                    <a href="singleJobs.html" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom:7px;" class="pad_right col-sm-6">
                        <div class="box product-image-wrapper">
                            <div class="single-products row">
                                <div class="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company2.png" alt="" />
                                    <br />
                                    <br />
                                    <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i class="fa fa-calendar-o fa-fw"></i> 15.07.2016</span>
                                </div>
                                <div class="productinfo col-sm-5 ">
                                    <h2>SSD kovalevyn asennus kannettavaan</h2> Tarjoamme SSD kovalevyn asennuksen 50 euron tasahintaan
                                            <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div class="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Kannettavat tietokoneet
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div class="single-products">
                                <div class="pull-left col-sm-8">
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div class="pull-right col-sm-4">
                                    <a href="#" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pad_right col-sm-6">
                        <div class="box product-image-wrapper">
                            <div class="single-products row">
                                <div class="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company3.png" alt="" />
                                    <br />
                                    <br />
                                    <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i class="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>
                                </div>
                                <div class="productinfo col-sm-5">
                                    <h2>Kameran linssin vaihto</h2> Tarjoamme kameran linssin vaihdon 20% alennuksella.
                                            <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div class="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Kamerat
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div class="single-products">
                                <div class="pull-left col-sm-8">
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div class="pull-right col-sm-4">
                                    <a href="#" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div style="margin-bottom:7px;" class="pad_right col-sm-6">
                        <div class="box product-image-wrapper">
                            <div class="single-products row">
                                <div class="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company7.png" alt="" />
                                    <br />
                                    <br />
                                    <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i class="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>
                                </div>
                                <div class="productinfo col-sm-5">
                                    <h2>Tietokoneen käyttöjärjestelmän päivitys</h2> Tarjoamme tietokoneen käyttöjärjestelmän päivityksen ostaessasi siihen yli 50 euron arvoisen osan.
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div class="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Kannettavat tietokoneet
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div class="single-products">
                                <div class="pull-left col-sm-8">
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div class="pull-right col-sm-4">
                                    <a href="#" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pad_right col-sm-6">
                        <div class="box product-image-wrapper">
                            <div class="single-products row">
                                <div class="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company5.png" alt="" />
                                    <br />
                                    <br />
                                    <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i class="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                                </div>
                                <div class="productinfo col-sm-5">
                                    <h2> Antennin korjaus.</h2> Antennin korjaus 20% alennuksella.
                                    <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div class="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Antennit
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div class="single-products">
                                <div class="pull-left col-sm-8">
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div class="pull-right col-sm-4">
                                    <a href="#" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="pad_right col-sm-6">
                        <div class="box product-image-wrapper">
                            <div class="single-products row">
                                <div class="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company6.png" alt="" />
                                    <br />
                                    <br />
                                    <span class="date" data-toggle="tooltip" title="Created at" class="nobr"><i class="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                                </div>
                                <div class="productinfo col-sm-5">
                                    <h2>Tevision korjaus</h2> Korjaamme televisioita markkinoiden halvimpaan hintaan.
                                    <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" class="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div class="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. televisiot
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />

                                </div>
                            </div>
                            <hr />
                            <div class="single-products">
                                <div class="pull-left col-sm-8">
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i class="fa fa-eye fa-fw"></i> 14</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i class="fa fa-user fa-fw"></i> 5</span>
                                    <span class="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i class="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div class="pull-right col-sm-4">
                                    <a href="#" class="btn btn-default preview pull-right"><i class="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="companyOffers.html" class="btn btn-success preview pull-right"> Katso lisää <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        );
    }
}
