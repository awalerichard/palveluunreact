import React from "react";
import { IndexLink, Link } from "react-router";

export default class HeaderAd extends React.Component {

    render() {

        return (
            <section id="slider">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 testimg">
                            <p class="testtext" style={{"color":"Whitesmoke", "display": "none"}}>We can add whatever we want over the image </p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
