import React from "react";
import { IndexLink, Link } from "react-router";
//import observer from "mobx-react";

//@observer
export default class ProblemContainer extends React.Component {

    render() {
        /*
        const {problems} = this.props.store
        const problemsList = consumers.map(problem => (
            <div key={problem._id} className="pad_right col-sm-6">
                <div className="box product-image-wrapper">
                    <div className="single-products row">
                        <div className="productinfo col-sm-4 ">
                            <img src="images/companyoffers/company1.png" alt="" />
                            <br />
                            <br />
                            <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i style="color:green;" className="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                        </div>
                        <div className="productinfo col-sm-5">
                            <h2>Matkapuhelimen näytön korjaus</h2> Tarjoamme matkapuhelimen näytön korjauksen 30% alennuksella.
                                            <br />
                            <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>

                        </div>

                        <div className="product-right-info col-sm-3">
                            <p>
                                Oulu, 90130 Finland
                                                <br />
                                <br /> Matkapuhelimet, Elektroniikka, Antennit.
                                            </p>
                            <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                        </div>
                    </div>
                    <hr />
                    <div className="single-products">
                        <div className="pull-left col-sm-8">
                            <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                            <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                            <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                        </div>
                        <div className="pull-right col-sm-4">
                            <a href="singleJobs.html" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                        </div>
                    </div>
                </div>
            </div>
        ))
        */
        return (
            <div className="recommended_items">
                <h2 className="title text-center">Yritysten tarjoukset</h2>

                <div className="viewmore1">
                    <div className="pad_right col-sm-6">
                        <div className="box product-image-wrapper">
                            <div className="single-products row">
                                <div className="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company1.png" alt="" />
                                    <br />
                                    <br />
                                    <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i style="color:green;" className="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                                </div>
                                <div className="productinfo col-sm-5">
                                    <h2>Matkapuhelimen näytön korjaus</h2> Tarjoamme matkapuhelimen näytön korjauksen 30% alennuksella.
                                            <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>

                                </div>

                                <div className="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Matkapuhelimet, Elektroniikka, Antennit.
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div className="single-products">
                                <div className="pull-left col-sm-8">
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div className="pull-right col-sm-4">
                                    <a href="singleJobs.html" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom:7px;" className="pad_right col-sm-6">
                        <div className="box product-image-wrapper">
                            <div className="single-products row">
                                <div className="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company2.png" alt="" />
                                    <br />
                                    <br />
                                    <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i className="fa fa-calendar-o fa-fw"></i> 15.07.2016</span>
                                </div>
                                <div className="productinfo col-sm-5 ">
                                    <h2>SSD kovalevyn asennus kannettavaan</h2> Tarjoamme SSD kovalevyn asennuksen 50 euron tasahintaan
                                            <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div className="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Kannettavat tietokoneet
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div className="single-products">
                                <div className="pull-left col-sm-8">
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div className="pull-right col-sm-4">
                                    <a href="#" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pad_right col-sm-6">
                        <div className="box product-image-wrapper">
                            <div className="single-products row">
                                <div className="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company3.png" alt="" />
                                    <br />
                                    <br />
                                    <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i className="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>
                                </div>
                                <div className="productinfo col-sm-5">
                                    <h2>Kameran linssin vaihto</h2> Tarjoamme kameran linssin vaihdon 20% alennuksella.
                                            <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div className="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Kamerat
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div className="single-products">
                                <div className="pull-left col-sm-8">
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div className="pull-right col-sm-4">
                                    <a href="#" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div style="margin-bottom:7px;" className="pad_right col-sm-6">
                        <div className="box product-image-wrapper">
                            <div className="single-products row">
                                <div className="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company7.png" alt="" />
                                    <br />
                                    <br />
                                    <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i className="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>
                                </div>
                                <div className="productinfo col-sm-5">
                                    <h2>Tietokoneen käyttöjärjestelmän päivitys</h2> Tarjoamme tietokoneen käyttöjärjestelmän päivityksen ostaessasi siihen yli 50 euron arvoisen osan.
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div className="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Kannettavat tietokoneet
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div className="single-products">
                                <div className="pull-left col-sm-8">
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div className="pull-right col-sm-4">
                                    <a href="#" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pad_right col-sm-6">
                        <div className="box product-image-wrapper">
                            <div className="single-products row">
                                <div className="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company5.png" alt="" />
                                    <br />
                                    <br />
                                    <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i className="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                                </div>
                                <div className="productinfo col-sm-5">
                                    <h2> Antennin korjaus.</h2> Antennin korjaus 20% alennuksella.
                                    <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div className="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. Antennit
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />
                                </div>
                            </div>
                            <hr />
                            <div className="single-products">
                                <div className="pull-left col-sm-8">
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div className="pull-right col-sm-4">
                                    <a href="#" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className="pad_right col-sm-6">
                        <div className="box product-image-wrapper">
                            <div className="single-products row">
                                <div className="productinfo col-sm-4 ">
                                    <img src="images/companyoffers/company6.png" alt="" />
                                    <br />
                                    <br />
                                    <span className="date" data-toggle="tooltip" title="Created at" className="nobr"><i className="fa fa-calendar-o fa-fw"></i> 01.06.2016</span>

                                </div>
                                <div className="productinfo col-sm-5">
                                    <h2>Tevision korjaus</h2> Korjaamme televisioita markkinoiden halvimpaan hintaan.
                                    <br />
                                    <span style="color:blue"><i style="margin-top:12px; color:red;" className="fa fa-comments-o fa-fw"></i> arvostelua 225</span>
                                </div>
                                <div className="product-right-info col-sm-3">
                                    <p>
                                        Oulu, 90130 Finland
                                                <br />
                                        <br /> Korjaustyöt. televisiot
                                            </p>
                                    <img style="margin-bottom:20px" src="images/product-details/rating.png" alt="" />

                                </div>
                            </div>
                            <hr />
                            <div className="single-products">
                                <div className="pull-left col-sm-8">
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Visits"><i className="fa fa-eye fa-fw"></i> 14</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Unique visitors"><i className="fa fa-user fa-fw"></i> 5</span>
                                    <span className="label label-default font-normal font-size-normal" data-toggle="tooltip" title="Term"><i className="fa fa-calendar fa-fw"></i> 5 päivää</span>
                                </div>
                                <div className="pull-right col-sm-4">
                                    <a href="#" className="btn btn-default preview pull-right"><i className="fa fa-file-o fa-fw"></i>Esikatselu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="companyOffers.html" className="btn btn-success preview pull-right"> Katso lisää <i className="fa fa-arrow-circle-right"></i></a>
            </div>
        );
        
    }
}
