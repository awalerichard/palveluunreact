import React from "react";
import { IndexLink, Link } from "react-router";
//import observer from "mobx-react";

//@observer
export default class SideFilter extends React.Component {
    render() {
        return (
            <div style="background-color:#ccc; border-radius: 5px; padding-left:0.5%; padding-right:0.5%; border: 1px solid rgba(60, 141, 188, 0.21);" class="col-sm-2">
                <div style="padding-left:0px; padding-right:0px;" class="left-sidebar">
                    <h2>ETSITKÖ?</h2><hr />
                    <form>
                        <input type="checkbox" name="Location" id="type" value="Car" checked />&nbsp;&nbsp;YRITYSTEN TARJOUKSET<br />
                        <input type="checkbox" name="Location" id="type" value="Bike" />&nbsp;&nbsp;Kuluttajien Tarjouspyynnöt<br />
                        <input type="checkbox" name="Location" id="type" value="Bike" />&nbsp;&nbsp;PALVELUYRITYKSET<br />
                    </form>
                    <div class="panel-group category-products" id="accordian">
              <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#state">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        ALUE
										</a>
                                </h4>
                            </div>
                            <div id="state" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="#">    <input type="checkbox" />Pohjois-Suomi </a></li>
                                        <li><a href="#">    <input type="checkbox" />Uusimaa </a></li>
                                        <li><a href="#">    <input type="checkbox" />Varsinais-Suomi </a></li>
                                        <li><a href="#">    <input type="checkbox" />Satakunta</a></li>
                                        <li><a href="#">	  <input type="checkbox" />Kanta-Häme</a></li>
                                        <li><a href="#">    <input type="checkbox" />Pirkanmaa</a></li>
                                        <li><a href="#">    <input type="checkbox" />Päijät-Häme</a></li>
                                        <li><a href="#">    <input type="checkbox" />Kymenlaakso</a></li>
                                        <li><a href="#">    <input type="checkbox" />Etelä-Karjala</a></li>
                                        <li><a href="#">    <input type="checkbox" />Etelä-Savo</a></li>
                                        <li><a href="#">  	<input type="checkbox" />Pohjois-Savo</a></li>
                                        <li><a href="#">  <input type="checkbox" />Pohjois-Karjala</a></li>
                                        <li><a href="#">	<input type="checkbox" />Keski-Suomi</a></li>
                                        <li><a href="#">	<input type="checkbox" />Etelä-Pohjanmaa</a></li>
                                        <li><a href="#">	<input type="checkbox" />Pohjanmaa</a></li>
                                        <li><a href="#">	<input type="checkbox" />Keski-Pohjanmaa</a></li>
                                        <li><a href="#">	<input type="checkbox" />Pohjois-Pohjanmaa</a></li>
                                        <li><a href="#">	<input type="checkbox" />Kainuu</a></li>
                                        <li><a href="#">	<input type="checkbox" />Lappi</a></li>
                                        <li><a href="#">	<input type="checkbox" />Ahvenanmaa - Åland</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#location">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        KAUPUNKI
										</a>
                                </h4>
                            </div>
                            <div id="location" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="#">Helsinki </a></li>
                                        <li><a href="#">Espoo </a></li>
                                        <li><a href="#">Tampere </a></li>
                                        <li><a href="#">Vantaa</a></li>
                                        <li><a href="#">Oulu </a></li>
                                        <li><a href="#">Turku </a></li>
                                        <li><a href="#">Jyväskylä </a></li>
                                        <li><a href="#">Lahti </a></li>
                                        <li><a href="#">Kuopio </a></li>
                                        <li><a href="#">Kouvola </a></li>
                                        <li><a href="#">Pori </a></li>
                                        <li><a href="#">Joensuu </a></li>
                                        <li><a href="#">Lappeenranta </a></li>
                                        <li><a href="#">Hämeenlinna </a></li>
                                        <li><a href="#">Vaasa </a></li>
                                        <li><a href="#">Rovaniemi </a></li>
                                        <li><a href="#">Seinäjoki </a></li>
                                        <li><a href="#">Mikkeli </a></li>
                                        <li><a href="#">Kotka </a></li>
                                        <li><a href="#">Salo </a></li>
                                        <li><a href="#">Porvoo </a></li>
                                        <li><a href="#">Kokkola </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#category">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Kategoriat
										</a>
                                </h4>
                            </div>
                            <div id="category" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="#">Elektroniikka</a></li>
                                        <li><a href="#">Kotitalouspalvelut</a></li>
                                        <li><a href="#">Hyvinvointipalvelut</a></li>
                                        <li><a href="#">Vapaa-aika</a></li>
                                        <li><a href="#">Asunnot ja tontit</a></li>
                                        <li><a href="#">Muu</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#brand">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Alakategoriat
										</a>
                                </h4>
                            </div>
                            <div id="brand" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="#">Tietokoneet</a></li>
                                        <li><a href="#">Matkapuhelimet</a></li>
                                        <li><a href="#">Kamerat</a></li>
                                        <li><a href="#">Kannettavat tietokoneet</a></li>
                                        <li><a href="#">Kodinkoneet</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#body_type">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Merkki
										</a>
                                </h4>
                            </div>
                            <div id="body_type" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="#">Samsung</a></li>
                                        <li><a href="#">iPhone</a></li>
                                        <li><a href="#">Nokia</a></li>
                                        <li><a href="#">HTC</a></li>
                                        <li><a href="#">Huawei</a></li>
                                        <li><a href="#">Sony</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Suodata</a></h4>
                            </div>
                        </div>
                    </div>

                    <form>
                        Etäisyys&nbsp;&nbsp;<input type="number" name="quantity" id="type" name="quantity" min="1" max="5" />&nbsp;&nbsp;&nbsp;&nbsp;KM<br />
                    </form>

                    <form>
                        Aloituspäivä &nbsp;&nbsp;
                        <input type="date" name="date_in" id="date_in" value="" /><br /><br />
                        Lopetuspäivä &nbsp;&nbsp;
                        <input type="date" name="date_in" id="date_out" value="" /><br />
                    </form>
                    <input type="submit" value="Etsi " class="btn btn-success btn-block" /><br /><br /><hr /><br />
                    <div class="advert">
                        <img src="images/home/ad/1.jpg" />
                        <img src="images/home/ad/4.jpg" />
                        <img src="images/home/ad/5.jpg" />
                        <img src="images/home/ad/6.jpg" />
                    </div>
                </div>
            </div>
        );
    }
}
