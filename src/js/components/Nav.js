import React from "react";
import { IndexLink, Link } from "react-router";
//import observer from "mobx-react";

//@observer
export default class Nav extends React.Component {
  render() {
    return (
      <div>
        <div class="header-middle">
          <div class="container-fluid">

            <div class="row">

              <div class="navbar navbar-default" style={{"borderRadius":0, "background": "none", "border": "none", "marginBottom": 0}}>

                <div class="navbar-header col-sm-2">
                  <a class="navbar-brand" href="index.html">Palveluun</a>
                </div>
                <div id="top" class="col-sm-7">
                  <ul class="nav navbar-nav ">
                    <li><a href="#">TÄYDELLINEN TEKNINEN KORJAUS</a></li>
                    <li><a href="#"><i class="fa fa-phone"> +358   440477559 </i></a></li>
                    <li><a href="#">info@palveluun.fi</a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  </ul>
                </div>
                <div id="top" class="col-sm-3">
                  <ul class="language nav navbar-nav pull-right " style={{"padding": "0 !important"}}>
                    <li><a href="login_user.html"><i class="fa fa-edit"></i> Rekisteröidy / Kirjaudu </a></li>
                    <li class=" subNav">
                      <a href="#" title="Switch language"> Suomi<i class="fa fa-caret-down"> &nbsp; </i></a>
                      <ul class="submenu" style={{"width":"90px", "marginLeft":"1px", "height": "55px"}}>
                        <li><a href="#"> Englanti</a></li>
                        <li><a href="#"> Ruotsi</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-middle">
          <div class="container-fluid">
            <ul class="nav navbar-nav" id="for_font">
              <li><a href="login_user.html" ><i class="fa fa-bell"></i>&nbsp;&nbsp;Pyydä palvelua ?</a></li>
              <li><a href="companyOffers.html"><i class="fa-rotate-270 glyphicon glyphicon-wrench"></i>&nbsp;&nbsp;Yritysten tarjoukset</a></li>
              <li><a href="clientProblems.html"><i class="fa fa-tasks"> </i>&nbsp;&nbsp;Kuluttajien Tarjouspyynnöt</a></li>
              <li><a href="networks.html"><i class="fa fa-users"></i>&nbsp;&nbsp;Verkostomme</a></li>
              <li><a href="blogs.html"><i class="fa fa-newspaper-o"></i>&nbsp;&nbsp;Blogi</a></li>
              <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i>&nbsp;&nbsp;Yleisimmin kysytyt kysymykset</a></li>
            </ul>
            <div class="row search-box-container" id="white">
              <div class="search_box">
                <input class="search_box" type="search" placeholder="Etsi" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
